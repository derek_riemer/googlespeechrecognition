# Google Speech Recognition #

* Autores: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez,
  colaboradores de NVDA.
* Descargar [versión estable][1]
* Descargar [versión de desarrollo][2]

Este complemento te capacita para utilizar el reconocimiento de voz para
escribir texto utilizando unas pocas teclas.

## Utilización ##

* Abre el diálogo de configuración para este complemento desde el menú de
  NVDA.
* De la larga lista de idiomas, selecciona los idiomas que hablas, y pulsa
  Aceptar.
* Al menos un idioma tiene que ser seleccionado en todo momento.
* Por ejemplo Inglés, Francés y Alemán.
* Ahora ya estás listo para utilizar el reconocimiento de voz, ve a
  cualquier campo de texto, como el navegador o un documento de texto.
* Pulsa NVDA+y para cambiar entre los idiomas configurados.
* Para comenzar el dictado, pulsa NVDA+shift+g, escucharás un pitido de tono
  alto.
* Comienza a hablar en el idioma elegido.
* Para detener el dictado, pulsa el mismo atajo de teclado, y esta vez se escuchará un pitido grave.
Nota: si no detienes la grabación, se detendrá automáticamente  después de 10 segundos.
Sólo teniendo en cuenta el intervalo de tiempo más corto, se tiene la mayor precisión.
* La voz se enviará al servidor de reconocimiento de habla de Google, y cuando se devuelva la transcripción, NVDA la verbalizará.
* Si el reconocimiento es correcto, pulsa el atajo de teclado para aceptar, NVDA+g, y  el texto se escribirá a la posición del cursor.
* Si el reconocimiento no fue correcto, simplemente vuelve a grabar pero utiliza un trozo más pequeño
de palabras. Puede que sea necesario dividir una frase larga en dos o tres trozos.
* Cuando se quiera hablar en un idioma diferente, simplemente cambia a uno de tus idiomas y repite los pasos.

## Órdenes de teclado ##

* NVDA+shift+g, Inicia/detiene la grabación.
* NVDA+g, acepta el resultado de la traducción.
* NVDA+y, cambia entre los idiomas elegidos.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr

[2]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
