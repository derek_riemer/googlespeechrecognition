# गुगल बोली पहिचान #

* लेखकहरू: मेसर हमिद , वेका गोजालिसभिलि, नोलिया रुइज्, नेत्रवाणी योगदान
  कर्ताहरू ।
* डाउनलोड[स्थिर संस्करण][1]
* डाउनलोड[विकास संस्करण][2]

This addon enables you to use speech recognition to write text using a few
keystrokes.

## Usage ##

* Open the configuration dialog for this addon from the NVDA menu.
* From the long list of languages, select the languages that you speak, and
  press ok.
* कुनै पनि बखतमा एउटा भाषाको चयन गरेको हुनु पर्छ ।
* उदाहरणका लागि फ्रेन्च र अङ्ग्रेजी.
* Now you are ready to use speech recognition, go to any text field such as
  in your browser or to a text document.
* Press NVDA+y to cycle between your configured languages.
* To start dictating, press NVDA+shift+g, you will hear a high pitched beep.
* Start speaking in the chosen language .
* To stop dictation, press the same shortcut, and this time you will hear a low pitched beep.
Note: if you do not stop the recording, it will automatically be stopped after 10 seconds.
Bare in mind the shorter time interval, the higher accuracy.
* The speech will be sent to the Google speech recognition service, and when the transcription is returned, NVDA will announce it.
* If the recognition is correct, press the accept shortcut, NVDA+g, and the text will be written to your cursor position.
* If the recognition was not correct, simply rerecord but use a smaller chunk
of words. You might need to split a long sentence into two or three chunks.
* When you want to speak in a different language, simply cycle to one of your languages and repeat the steps.

## Key Commands ##

* NVDA+shift+g, Start/stop recording.
* NVDA+g, accept transcription result.
* NVDA+y, Cycle between your chosen languages.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr

[2]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
