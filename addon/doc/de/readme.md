# Google Spracherkennung #

* Autoren: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez, NVDA
  contributors.
* [Stabile Version herunterladen][1]
* [testversion][2]

Diese Erweiterung ermöglicht Ihnen, die Spracherkennung von Google innerhalb
von NVDA mit nur wenigen Tastenkombinationen zu verwenden.

## Verwendung ##

* Öffnen Sie den Konfigurationsdialog für die Google-Spracherkennung im Menü
  "Einstellungen" im NVDA-Menü.
* Wählen Sie die Sprache(n) aus, die Sie sprechen und bestätigen Sie die
  auswahl mit "ok" Drücken Sie dazu die Leertaste auf jeder Sprache, die Sie
  hinzufügen möchten; der Statur sollte sich von "nicht hinzugefügt" auf
  "hinzugefügt" ändern.
* Es muss mindestens eine Sprache ausgewählt sein.
* Wählen Sie z.B. englisch, Französisch und deutsch aus.
* Um die spracherkennung zu verwenden, öffnen Sie ein beliebiges Textfeld
  (ein Eingabefeld in einem Browser oder einen Texteditor)
* Drücken Sie nvda+y, um zwischen Ihren hinzugefügten Sprachen zu wechseln.
* Um das Diktat zu beginnen, drücken Sie nvda+Umschalt+g; Sie werden einen
  hohen Signalton hören.
* Diktieren Sie den Text in der eingestellten Sprache
* Um das Diktat zu beenden, drücken Sie die Tastenkombination erneut; sie werden diesmal einen tiefen Signalton hören.
Bedenken Sie dass das Diktat automatisch nach 10 Sekunden beendet wird, wenn Sie es nicht von Hand beenden.
Je kürzer das Diktat ist, desto höher ist die Genauigkeit bei der Erkennung.
* Die Sprache wird an den Google-Spracherkennungsdienst geschickt und NVDA wird das Transscript anzeigen, sobald die Erkennung abgeschlossen ist.
* Wenn Sie mit dem Ergebnis zufrieden sind drücken Sie die Tastenkombination  NVDA+g, um dieses zu akzeptieren; der Text wird an der aktuellen Cursorposition eingefügt.
* Wenn sie mit dem Ergebnis nicht zufrieden sind, müssen Sie möglicherweise ihr Diktat aufteilen (z.B. die Teilsätze von längeren Sätzen einzeln einsprechen)
* Wenn Sie in einer anderen Sprache diktieren möchten, wechseln Sie einfach die Sprache und widerholen Sie die Schritte.

## Tastenkombinationen ##

* NVDA+Umschalt+g, beginnt/beendet das Diktat
* NVDA+g, akzeptiert das Übersetzungsergebmis.
* NVDA+y, wechselt zwischen den hinzugefügten Sprachen

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr

[2]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
