# Google rozpoznávanie reči #

* Autori: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez, Tím NVDA.
* Stiahnuť [stabilná verzia][1]
* Stiahnuť [vývojová verzia][2]

Tento doplnok vám umožní písať text pomocou rozpoznávania reči tak, že
budete musieť stlačiť len pár klávesových skratiek.

## Použitie ##

* otvorte okno s nastaveniami pre tento doplnok z menu NVDA.
* Zo zoznamu všetkých jazykov vyberte tie, ktorými hovoríte a stlačte
  tlačidlo OK.
* Vždy musí byť vybratý aspoň jeden jazyk.
* Povedzme, že ste si vybrali Slovenčinu, Češtinu a Angličtinu.
* Teraz je už všetko nastavené a stačí, ak sa presuniete do nejakého
  textového poľa v internetovom prehliadači alebo v otvorenom dokumente.
* Na prepínanie medzi vybratými jazykmi stlačte nvda+y.
* Aby ste mohli nadyktovať text, stlačte nvda+shift+g. Budete počuť vysoký
  tón.
* Začnite hovoriť vo vybratom jazyku.
* Diktovanie zastavíte tou istou skratkou. Budete počúť nižší tón.
Všimnite si, že ak nezastavíte nahrávanie, nahrávanie sa automaticky zastaví po desiatich sekundách.
Vezmite na vedomie, že čím kratšia nahrávka, tým lepšia je presnosť rozpoznaného textu.
* Nahrávka bude odoslaná do služby google a keď Google vráti prepis, NVDA ho prečíta.
* Ak je rozpoznaný text správny, môžete ho vložiť na pozíciu kurzora skratkou nvda+g.
* Ak text nebol rozpoznaný správne, nahrajte ho znovu a skúste nadiktovať text po častiach.
Dlhú vetu by ste mali rozdeliť aj na dve-tri časti.
* Ak chcete diktovať v inom jazyku, jednoducho vyberte iný jazyk.

## Klávesové skratky ##

* Nvda+shift+g, spusti alebo zastav nahrávanie.
* NVDA+g, zapíš prepis.
* NVDA+y, prepína medzi vybratými jazykmi.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr

[2]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
