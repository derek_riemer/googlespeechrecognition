# encoding: utf-8
import sys
import os
import tempfile
import wave
import time
import threading
impPath = os.path.abspath(os.path.dirname(__file__))
sys.path.append(impPath)
import plumbum
import json
import urllib2
import pyaudio
from plumbum import local
del sys.path[-1]

flac = local[os.path.join(os.path.dirname(__file__), "flac.exe")]

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000

class spk2txt(threading.Thread):

    def __init__(self, lang='en-GB', wantResults=10, *args, **kwargs):
        super(spk2txt, self).__init__(*args, **kwargs)
        self.p = pyaudio.PyAudio()
        self.frames = []
        self._stop = threading.Event()
        self.lang = lang
        self.wantResults = wantResults
        self.rawData = ""

    def stop(self):
        self._stop.set()

    def run(self):
        outfn = tempfile.mktemp()
        stream = self.p.open(format=FORMAT,
            channels=CHANNELS,
            rate=RATE,
            input=True,
            frames_per_buffer=CHUNK)
        while not self._stop.isSet():
            data = stream.read(CHUNK)
            self.frames.append(data)
            time.sleep(0)


        stream.stop_stream()
        stream.close()

        wf = wave.open(outfn+".wav", 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(self.p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b''.join(self.frames))
        wf.close()

        flac['-f', '-o', outfn+'.flac', outfn+'.wav']()
        f = open(outfn+'.flac', 'rb')
        flacData = f.read()
        f.close()

        api_key = 'AIzaSyCnl6MRydhw_5fLXIdASxkLJzcJh5iX0M4'
        baseUrl = 'https://www.google.com/speech-api/v2/recognize'
        url = '{url}?key={key}&xjerr=1&pfilter=1&client=chromium&lang={lang}&maxresults={wanted}'.format(url=baseUrl, key=api_key, lang=self.lang, wanted=self.wantResults)
        header = {'Content-Type' : 'audio/x-flac; rate=16000'}
        req = urllib2.Request(url, flacData, header)
        conn = urllib2.urlopen(req)
        self.rawData = conn.read()

    def processResults(self):
        lines = self.rawData.split('\n')
        if '' in lines: lines.remove('')
        for line in lines:
            record = json.loads(line)
            try:
                result_index = record['result_index']
            except:
                pass
            results = record['result']
            for result in results:
                if 'final' in result:
                    print( "final found")
                    alternatives = result['alternative']
                    myTranscripts = []
                    for alternative in alternatives:
                        if 'transcript' in alternative:
                            myTranscripts.append(alternative['transcript'])
                    return myTranscripts
        return []

    def __del__self(self):
        self.p.terminate()

