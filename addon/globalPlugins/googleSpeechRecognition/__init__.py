# encoding: utf-8

# Speech Recognition powered by Google
# A global plugin for NVDA
# Copyright 2013 NVDA contributors, released under GPL.
# See copying.txt for license information.

from cStringIO import StringIO
from configobj import ConfigObj
from validate import Validator
import os
#import config
import gui
import wx
import brailleInput
import braille
import globalPluginHandler
import globalVars
import ui
import tones
import addonHandler
from functools import wraps
from spk2txt import spk2txt
from ring import Ring
from logHandler import log
from langslist import langsList

# The root of the addon folder
_addonDir = os.path.join(os.path.dirname(__file__), "..", "..").decode("mbcs")
_curAddon = addonHandler.Addon(_addonDir)
_addonSummary = _curAddon.manifest['summary']

addonHandler.initTranslation()

config_FileName = os.path.join(globalVars.appArgs.configPath, "googleSpeechRecognition.ini")

confspec = ConfigObj(StringIO("""#Configuration file

[Language settings]
	lang = string(default="en_US")
	ringLangs = list(default=list("en_US"))
"""), encoding="UTF-8", list_values=False)
confspec.newlines = "\r\n"
conf = ConfigObj(config_FileName, configspec = confspec, indent_type = "\t", encoding="UTF-8")
val = Validator()
conf.validate(val)
addedLanguages = conf["Language settings"]["ringLangs"]
if conf["Language settings"]["lang"] in addedLanguages:
	lng = conf["Language settings"]["lang"]
else:
	lng = addedLanguages[0]

def finally_(func, final):
	"""Calls final after func, even if it fails."""
	def wrap(f):
		@wraps(f)
		def new(*args, **kwargs):
			try:
				func(*args, **kwargs)
			finally:
				final()
		return new
	return wrap(final)

class Language:

	def __init__(self, code, dicNames, checkStatusList):
		self.code = code
		self.name = dicNames[code]
		if code in checkStatusList:
			# Translators: announced when the user selects one of the supported recognition languages.
			self.status = _("Added")
		else:
			# Translators: announced when the user unselects one of the supported recognition languages.
			self.status = _("Not added")

class LanguagesDialog(gui.SettingsDialog):

		# Translators: The title of the languages dialog.
	title = _("Google speech recognition languages: %d") % len(langslist.langsCodes)

	def makeSettings(self, settingsSizer):
		entriesSizer=wx.BoxSizer(wx.VERTICAL)
		# Translators: the label for the languages list for Google speech recognition
		entriesLabel=wx.StaticText(self, -1, label=_("&Add the languages you want to use for speech recognition. Press Space to toggle between added or not added."))
		entriesSizer.Add(entriesLabel)
		self.languagesList=wx.ListCtrl(self,-1,style=wx.LC_REPORT|wx.LC_SINGLE_SEL,size=(550, 350))
		# Translators: The label for a column in languages list used to identify a language code.
		self.languagesList.InsertColumn(0,_("Code"), width=100)
		# Translators: The label for a column in languages list used to identify language name.
		self.languagesList.InsertColumn(1, _("Name"), width=300)
		# Translators: The label for a column in languages list used to identify language status.
		self.languagesList.InsertColumn(2,_("Status"), width=150)
		self.languagesList.Bind(wx.EVT_LIST_ITEM_FOCUSED, self.onListItemSelected)
		self.languagesList.Bind(wx.EVT_CHAR, self.onListChar)
		entriesSizer.Add(self.languagesList, proportion=8)
		self.refreshLanguagesList(0)

	def postInit(self):
		self.languagesList.SetFocus()

	def OnToggleClick(self, evt):
		try:
			langCode = langslist.langsCodes[self.index]
		except:
			return
		global addedLanguages
		if langCode not in addedLanguages:
			addedLanguages.append(langCode)
		elif len(addedLanguages) == 1:
			wx.CallAfter(gui.messageBox,
			# Translators: The user has unselected the last language from the available languages, so display an error.
			_("Can not remove last selected language, you need at least one language for recognition to work."),
			# Translators: title of error dialog.
			_("Error removing selected language"),
			wx.OK|wx.ICON_ERROR)
		else:
			addedLanguages.remove(langCode)
		self.refreshLanguagesList(self.index)

	def onListItemSelected(self, evt):
		self.index = evt.GetIndex()

	def saveDefaultLanguages(self):
		conf["Language settings"]["lang"] = lng
		conf["Language settings"]["ringLangs"] = addedLanguages
		try:
			conf.validate(val, copy=True)
			conf.write()
			log.info("googleSpeechRecognition add-on configuration saved")
		except Exception, e:
			log.warning("Could not save googleSpeechRecognition add-on configuration")
			log.debugWarning("", exc_info=True)
			#raise e

	def onOk(self, evt):
		super(LanguagesDialog, self).onOk(evt)
		if len(addedLanguages) > 0:
			self.saveDefaultLanguages()

	def refreshLanguagesList(self, activeIndex):
		self.languagesList.DeleteAllItems()
		for code in langslist.langsCodes:
			language = Language(code, langsList, addedLanguages)
			self.languagesList.Append((language.code, language.name, language.status))
		self.languagesList.Select(activeIndex,on=1)
		self.languagesList.SetItemState(activeIndex,wx.LIST_STATE_FOCUSED,wx.LIST_STATE_FOCUSED)

	def onListChar(self, evt):
		if evt.KeyCode == wx.WXK_RETURN:
			# Activate the OK button.
			self.ProcessEvent(wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
		elif evt.KeyCode == wx.WXK_SPACE:
			self.OnToggleClick(None)
		else:
			evt.Skip()

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	scriptCategory = unicode(_addonSummary)

	def __init__(self, *args, **kwargs):
		super(GlobalPlugin, self).__init__(*args, **kwargs)
		if globalVars.appArgs.secure:
			return
		self.recognizer = None
		self.results = []
		self.timer = None
		self.toggling = False
		self.myLangs = Ring(addedLanguages)
		self.createMenu()

	def getScript(self, gesture):
		if not self.toggling:
			return globalPluginHandler.GlobalPlugin.getScript(self, gesture)
		script = globalPluginHandler.GlobalPlugin.getScript(self, gesture)
		if not script:
			script = finally_(self.script_error, self.finish)
		return finally_(script, self.finish)

	def finish(self):
		self.toggling = False
		self.clearGestureBindings()
		self.bindGestures(self.__gestures)

	def script_error(self, gesture):
		tones.beep(120, 100)

	def script_GSRLayer(self, gesture):
		# A run-time binding will occur from which we can perform various layered translation commands.
		# First, check if a second press of the script was done.
		if self.toggling:
			self.script_error(gesture)
			return
		self.bindGestures(self.__GSRGestures)
		self.toggling = True
		tones.beep(100, 10)
	script_GSRLayer.__doc__=_("Google Speech Recognition layer commands.")

	def createMenu(self):
		self.prefsMenu = gui.mainFrame.sysTrayIcon.menu.GetMenuItems()[0].GetSubMenu()
		self.googleSpeechRecognitionSettingsItem = self.prefsMenu.Append(wx.ID_ANY,
			# Translators: name of the option in the menu.
			_("Google &Speech Recognition Settings..."),
			# Translators: tooltip text for the menu item.
			_("Select languages to be used for recognition."))
		gui.mainFrame.sysTrayIcon.Bind(wx.EVT_MENU, self.onSettings, self.googleSpeechRecognitionSettingsItem)

	def terminate(self):
		try:
			self.prefsMenu.RemoveItem(self.googleSpeechRecognitionSettingsItem)
		except wx.PyDeadObjectError:
			pass

	def onSettings(self, evt):
		if gui.isInMessageBox:
			return
		gui.mainFrame.prePopup()
		d = LanguagesDialog(gui.mainFrame)
		d.Show()
		gui.mainFrame.postPopup()

	def stopRecord(self):
		if not self.recognizer:
			return
		if self.timer:
			self.timer.Stop()
			self.timer = None
		self.recognizer.stop()
		tones.beep(500,300)
		self.recognizer.join()
		self.results = self.recognizer.processResults()
		self.recognizer = None
		if len(self.results) == 0:
			# Translators: message presented when the translation service did not return any results.
			ui.message(_("Sorry, no recognition results."))
			return
		# Translators: message presented when processing results has been finished.
		messages = [_("Processing finished")]
		keyLabels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
		for i in range(min(len(keyLabels), len(self.results))):
			messages.append(u"{number}: {transcript}".format(number=keyLabels[i], transcript=self.results[i]))
		ui.message("\n".join(messages))

	def script_cycleLang(self, gesture):
		self.myLangs.next()
		global lng
		lng = self.myLangs.__str__()
		ui.message(langsList[lng])
		self.saveCurLang()
	script_cycleLang.__doc__=_("Cycles through the languages selected in addon settings.")

	def saveCurLang(self):
		conf["Language settings"]["lang"] = lng
		try:
			conf.validate(val, copy=True)
			conf.write()
			log.info("googleSpeechRecognition add-on configuration saved")
		except Exception, e:
			log.warning("Could not save googleSpeechRecognition add-on configuration")
			log.debugWarning("", exc_info=True)
			#raise e

	def script_record(self,gesture):
		if not self.recognizer:
			self.recognizer = spk2txt(lng.encode("UTF-8"))
			self.recognizer.start()
			tones.beep(1000,300)
			braille.handler.message(_("Google speech recognition running"))
			self.timer = wx.CallLater(10000, self.stopRecord)
		else:
			self.stopRecord()
	script_record.__doc__=_("Pressing this key starts recording, pressing it again stops recording, alternatively recording will be stopped automatically in 10 seconds .")

	def script_accept(self,gesture):
		try:
			index = int(gesture.keyName[-1])
		except AttributeError:
			index = int(gesture.mainKeyName[-1])
		if index == 0:
			index += 10
		# -1 because user starts counting at 1, python lists start at 0.
		index -= 1
		if len(self.results) == 0:
			# Translators: message presented when the translation service did not return any results.
			ui.message(_("Sorry, no recognition results."))
			return
		if index >= len(self.results):
			# Translators: the translation service did not give us so many results.
			# For example only 3 results were returned, but user is asking for the 5th alternative.
			ui.message(_("Recognition alternative not available."))
			return
		# Find out if the chosen message should be presented or inserted into document.
		if not "shift" in gesture.modifierNames:
			ui.message(self.results[index])
		else:
			self.finish()
			for mychar in self.results[index]:
				brailleInput.handler.sendChars(mychar)
	script_accept.__doc__=_("Pastes recognised text into edit fields.")

	__GSRGestures={
		"kb:r":"record",
		"kb:y":"cycleLang",
		"kb:1":"accept",
		"kb:2":"accept",
		"kb:3":"accept",
		"kb:4":"accept",
		"kb:5":"accept",
		"kb:6":"accept",
		"kb:7":"accept",
		"kb:8":"accept",
		"kb:9":"accept",
		"kb:0":"accept",
		"kb:shift+1":"accept",
		"kb:shift+2":"accept",
		"kb:shift+3":"accept",
		"kb:shift+4":"accept",
		"kb:shift+5":"accept",
		"kb:shift+6":"accept",
		"kb:shift+7":"accept",
		"kb:shift+8":"accept",
		"kb:shift+9":"accept",
		"kb:shift+0":"accept",
	}

	__gestures = {
		"kb:NVDA+shift+g": "GSRLayer",
	}
